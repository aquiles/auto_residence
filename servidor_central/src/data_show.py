import socket
import sys


def show_data():
	# signal.signal(signal.SIGINT, clean)

	TCP_IP = "192.168.0.52"
	TCP_PORT = 5000
	BUFFER_SIZE = 30

	
	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock_data:
		try:
			sock_data.connect((TCP_IP, TCP_PORT))
		except Exception as e:
			print("erro no connect")
			raise e

		# s.send(MESSAGE)
		while True:
			try:
				data = sock_data.recv(BUFFER_SIZE)
			except Exception as e:
				print("erro no recebimento")
				raise e
			res = str(data).split()
			try:
				temp = res[0][2:]
				umid = res[1][:6]
				print(f"Temperatura {temp}\nUmidade {umid}")
			except:
				print(f"dado chegou em formato fora do esperado \n dado:{data}")
				break
