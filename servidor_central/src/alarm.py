import socket
import sys
import subprocess


def alarm():
	TCP_IP = str(socket.INADDR_ANY)
	TCP_PORT = 5001
	BUFFER_SIZE = 30

	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock_alarm:
		sock_alarm.bind((TCP_IP, TCP_PORT))
		sock_alarm.listen(1)
		conn_alarm, addr = sock_alarm.accept()
		with conn_alarm:
			while 1:
				data = conn_alarm.recv(BUFFER_SIZE)
				if not data:
					print("erro no recebimento")
					break
				subprocess.run(["omxplayer", "All_Megaman_X_WARNING.mp3"])
	print("thread do alarme terminada")
