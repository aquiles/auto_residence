#!/usr/bin/python3

import threading
import signal
import sys
from data_show import *
from alarm import *
#from thread_trace import Thread_with_trace


def clean(*args):
	print("\nfechando")
	#thread_data.kill()
	#thread_alarm.kill()
	raise SystemExit


#signal.signal(signal.SIGINT, clean)
# global thread_data
# global thread_alarm
thread_data = threading.Thread(target=show_data)
thread_alarm = threading.Thread(target=alarm)

thread_data.start()
thread_alarm.start()
# signal.pause()

thread_data.join()
print("\npressione ctrl + c até encerrar o programa caso não tenha encerrado e não tenha tocado nenhum alarme\n")
thread_alarm.join() 

