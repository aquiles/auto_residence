# auto_residence

Foi feito somente o envio da temperatura e umidade via conexão tcp, e a implementação do alarme quando uma porta é aberta,janela é aberta ou uma presença é detectada.

Ao começar a usar faça questão de nenhum sensor de presença ou abertura de portas e janelas esteja previamente acionado.

## como usar
Inicie primeiro o servidor distribuído(na pasta servidor_distribuido) compilando com make e executando o execultavel na pasta bin. ex:

	make
	bin/bin

Depois inicie o servidor central(na pasta servidor_central) digitando:

	python3 src/main.py