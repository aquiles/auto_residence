#ifndef BCM_H_
#define BCM_H_

#include <bcm2835.h>
#include <stdlib.h>
#include <sched.h>
#include <sys/mman.h>
#include <pthread.h>
#include "client_tcp.h"

#define L1 RPI_V2_GPIO_P1_11
#define L2 RPI_V2_GPIO_P1_12
#define L3 RPI_V2_GPIO_P1_13
#define L4 RPI_V2_GPIO_P1_15
#define A1 RPI_V2_GPIO_P1_16
#define A2 RPI_V2_GPIO_P1_18
#define SP1 RPI_V2_GPIO_P1_22
#define SP2 RPI_V2_GPIO_P1_37
#define SA1 RPI_V2_GPIO_P1_29
#define SA2 RPI_V2_GPIO_P1_31
#define SA3 RPI_V2_GPIO_P1_32
#define SA4 RPI_V2_GPIO_P1_36
#define SA5 RPI_V2_GPIO_P1_38
#define SA6 RPI_V2_GPIO_P1_40

void init_bcm();
void * polling(void * args);

#endif