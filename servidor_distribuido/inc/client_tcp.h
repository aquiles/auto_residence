#ifndef CLIENT_TCP_H_
#define CLIENT_TCP_H_

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void client_tcp(char mensagem[]);

#endif