#ifndef SERVIDOR_TCP_H_
#define SERVIDOR_TCP_H_

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "sensor.h"

////////// GLOBAL ////////////
int servidorSocket;
int socketCliente_servidor;
//struct bme280_data sensor;
////////////////////////////
void TrataClienteTCP(int socketCliente_servidor);
void * servidor_tcp(void *args);
#endif