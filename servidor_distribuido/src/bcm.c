#include "bcm.h"

void init_bcm(){
	if (!bcm2835_init())
		exit(1);

	// Set the pins to be an INPUT
	bcm2835_gpio_fsel(SP1, BCM2835_GPIO_FSEL_INPT);
	bcm2835_gpio_fsel(SP2, BCM2835_GPIO_FSEL_INPT);
	bcm2835_gpio_fsel(SA1, BCM2835_GPIO_FSEL_INPT);
	bcm2835_gpio_fsel(SA3, BCM2835_GPIO_FSEL_INPT);
	bcm2835_gpio_fsel(SA4, BCM2835_GPIO_FSEL_INPT);
	bcm2835_gpio_fsel(SA5, BCM2835_GPIO_FSEL_INPT);
	bcm2835_gpio_fsel(SA6, BCM2835_GPIO_FSEL_INPT);
}
void * polling(void * args)
{
	const struct sched_param priority = {1};
    sched_setscheduler(0, SCHED_FIFO, &priority);
    // Trava o processo na memória para evitar SWAP
    mlockall(MCL_CURRENT | MCL_FUTURE);

	while(1)
	{
		if(bcm2835_gpio_lev(SP1)){
			client_tcp("SP1");
			pthread_exit(0);
		}
		if(bcm2835_gpio_lev(SP2)){
			client_tcp("SP2");
			pthread_exit(0);
		}
		if(bcm2835_gpio_lev(SA1)){
			client_tcp("SA1");
			pthread_exit(0);
		}
		if(bcm2835_gpio_lev(SA2)){
			client_tcp("SA2");
			pthread_exit(0);
		}
		if(bcm2835_gpio_lev(SA3)){
			client_tcp("SA3");
			pthread_exit(0);
		}
		if(bcm2835_gpio_lev(SA4)){
			client_tcp("SA4");
			pthread_exit(0);
		}
		if(bcm2835_gpio_lev(SA5)){
			client_tcp("SA5");
			pthread_exit(0);
		}
		if(bcm2835_gpio_lev(SA6)){
			client_tcp("SA6");
			pthread_exit(0);
		}
		usleep(500000);
	}
}
