#include <pthread.h>
#include "servidor_tcp.h"
#include "sensor.h"
#include "bcm.h"


/******** global var ********/
static int8_t i2c_fd;
pthread_t tcp_thread, polling_thread;

/********** function **************/

void clean(int sig)
{
	printf("\nfechando ...\n");
	close(socketCliente_servidor);	
	close(servidorSocket);
	int res = pthread_cancel(tcp_thread);
	if (res != 0) 
	{
		perror("Não foi possível cancelar a thread!");
		exit(EXIT_FAILURE);
	}
	res = pthread_cancel(polling_thread);
	exit(0);
}
/*********** main ***********/
int main()
{
	signal(SIGINT, clean);
	
	init_bcm();

	struct bme280_data sensor, *ptr_sensor;
	ptr_sensor = &sensor;
	float temp_ref_bottom, temp_ref_ceil;
	
	pthread_create (&tcp_thread, NULL, &servidor_tcp, 0);
	pthread_create (&polling_thread, NULL, &polling, 0);
	main_bme();
	return 0;
}
