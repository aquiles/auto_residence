#include "servidor_tcp.h"

////////// GLOBAL ////////////
int servidorSocket;
int socketCliente_servidor;
//struct bme280_data sensor;
////////////////////////////
void TrataClienteTCP(int socketCliente_servidor)
{
	char buffer[30];
	while (1)
	{
		sprintf(buffer,"%0.2lfC %0.2lf%%", bme_data[0], bme_data[1]);
		if (send(socketCliente_servidor, buffer, strlen(buffer) +1, 0) == -1)
		{
			printf("Erro no envio - send()\n");
			break;
		}
		sleep(1);

	}
}
void * servidor_tcp(void *args)
{
	struct sockaddr_in servidorAddr;
	struct sockaddr_in clienteAddr;
	unsigned short servidorPorta;
	unsigned int clienteLength;


	servidorPorta = 5000; 
	// Abrir Socket
	if ((servidorSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	{
		printf("falha no socker do Servidor\n");
		exit(1);
	}
	// Zerando a estrutura de dados
	memset(&servidorAddr, 0, sizeof(servidorAddr));

	// Montar a estrutura sockaddr_in
	servidorAddr.sin_family = AF_INET;
	servidorAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servidorAddr.sin_port = htons(servidorPorta);
	// Bind
	if (bind(servidorSocket, (struct sockaddr *)&servidorAddr, sizeof(servidorAddr)) < 0)
	{
		printf("Falha no Bind\n");
		exit(1);
	}
	// Listen
	if (listen(servidorSocket, 10) < 0)
	{
		printf("Falha no Listen\n");
		exit(1);
	}
	while (1)
	{
		clienteLength = sizeof(clienteAddr);
		if ((socketCliente_servidor = accept(servidorSocket, (struct sockaddr *)&clienteAddr, &clienteLength)) < 0)
			printf("Falha no Accept\n");
		// printf("Conexão do Cliente %s\n", inet_ntoa(clienteAddr.sin_addr));
		TrataClienteTCP(socketCliente_servidor);
		close(socketCliente_servidor);
	}
}
